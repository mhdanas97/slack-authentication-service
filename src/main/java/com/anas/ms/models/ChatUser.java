package com.anas.ms.models;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
public class ChatUser {

    @Id
    @GeneratedValue
    private long id;

    @Getter
    @Column(nullable = false, unique = true)
    private String username;

    @Getter
    @Setter
    @Column(nullable = false)
    private String password;

    @Getter
    @ManyToMany
    @Setter
    @JoinTable(name = "channel_subscription",
            joinColumns = @JoinColumn(name = "chat_user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "channel_id", referencedColumnName = "id"))
    private List<Channel> channels;

}
