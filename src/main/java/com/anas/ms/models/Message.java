package com.anas.ms.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@NoArgsConstructor
public class Message {
    @Id
    @GeneratedValue
    private long id;

    @Lob
    @Column
    @Getter
    private String messageContent;

    @ManyToOne
    @JoinColumn(name = "channel_id")
    private Channel channel;

    @ManyToOne
    @JoinColumn(name = "sender_id")
    @Getter
    private ChatUser sender;

    @Getter
    private Timestamp timestamp;

    @Lob
    @Column
    @Setter
    private byte[] attachment;

    public Message(String messageContent, Channel channel, ChatUser sender, Timestamp timestamp, byte[] attachment) {
        this();
        this.messageContent = messageContent;
        this.channel = channel;
        this.sender = sender;
        this.timestamp = timestamp;
        this.attachment = attachment;
    }
}
