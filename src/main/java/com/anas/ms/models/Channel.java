package com.anas.ms.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
public class Channel {

    @Id
    @GeneratedValue
    private long id;

    @Column(unique = true, nullable = false)
    @Getter
    private String name;

    @Getter
    @Setter
    @ManyToMany(mappedBy = "channels")
    private List<ChatUser> users;

    @Getter
    @OneToMany(mappedBy = "channel")
    private List<Message> messages;
}
