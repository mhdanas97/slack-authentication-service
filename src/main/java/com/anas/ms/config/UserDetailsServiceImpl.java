package com.anas.ms.config;

import com.anas.ms.models.ChatUser;
import com.anas.ms.repositories.ChatUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import static java.util.Collections.emptyList;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    ChatUserRepository chatUserRepository;

    public UserDetailsServiceImpl(ChatUserRepository chatUserRepository) {
        this.chatUserRepository = chatUserRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        ChatUser chatUser = chatUserRepository.findByUsernameIgnoreCase(username);

        if (chatUser == null)
            throw new UsernameNotFoundException(username);

        return new User(chatUser.getUsername(), chatUser.getPassword(), emptyList());
    }
}
