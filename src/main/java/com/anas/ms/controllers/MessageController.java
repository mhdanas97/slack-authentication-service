package com.anas.ms.controllers;

import com.anas.ms.exceptions.ChannelNotFoundException;
import com.anas.ms.models.Channel;
import com.anas.ms.models.ChatUser;
import com.anas.ms.models.Message;
import com.anas.ms.repositories.ChannelRepository;
import com.anas.ms.repositories.ChatUserRepository;
import com.anas.ms.repositories.MessageRepository;
import lombok.Getter;
import lombok.Setter;
import lombok.val;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.sql.Timestamp;

@RestController
@RequestMapping("/message")
public class MessageController {
    private static final Logger log = LoggerFactory.getLogger(MessageController.class);

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private ChannelRepository channelRepository;

    @Autowired
    private ChatUserRepository chatUserRepository;

    @PostMapping("/send")
    public MessageDto sendMessage(@RequestBody MessageDto message, Principal principal) {

        log.info("Send new message User<{}>, {}", principal.getName(), message);

        val user = chatUserRepository.findByUsernameIgnoreCase(message.getSender() != null ?
                message.getSender() : principal.getName());

        val channel = getChannel(message.getChannel(), user);
        val message2Add = new Message(message.getContent(),
                channel, user, new Timestamp(System.currentTimeMillis()), message.getAttachment());
        messageRepository.save(message2Add);
        message.setStatus("Sent");

        log.info("Send new message done with response {}", message);

        return message;
    }

    @PostMapping("/attachment")
    public ResponseEntity<String> attachData(@RequestParam(value = "channel") String channelName,
                                             @RequestParam(value = "file") String file,
                                             Principal principal) {
        val user = chatUserRepository.findByUsernameIgnoreCase(principal.getName());
        val message = messageRepository.findFirstByChannelAndSenderOrderByTimestampDesc(getChannel(channelName, user),
                user);
        message.setAttachment(file.getBytes());
        messageRepository.save(message);

        log.info("Add attachment to the message {}", message);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    private Channel getChannel(String channelName, ChatUser user) {
        val channel = channelRepository.findByNameIgnoreCase(channelName);
        if (channel == null || !user.getChannels().contains(channel))
            throw new ChannelNotFoundException("Channel not found or you're not subscribed");
        return channel;
    }

    private static class MessageDto {
        @Getter
        private String content;

        @Getter
        private String channel;

        @Getter
        private byte[] attachment;

        @Getter
        private String sender;

        @Setter
        private String status;
    }
}
