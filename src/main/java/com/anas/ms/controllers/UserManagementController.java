package com.anas.ms.controllers;

import com.anas.ms.models.ChatUser;
import com.anas.ms.repositories.ChatUserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserManagementController {
    private static final Logger log = LoggerFactory.getLogger(UserManagementController.class);

    @Autowired
    ChatUserRepository chatUserRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @PostMapping("/sign-up")
    public String signUp(@RequestBody ChatUser user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        chatUserRepository.save(user);
        return "added";
    }
}
