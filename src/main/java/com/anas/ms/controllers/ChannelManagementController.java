package com.anas.ms.controllers;

import com.anas.ms.exceptions.ChannelNotFoundException;
import com.anas.ms.models.Channel;
import com.anas.ms.models.ChatUser;
import com.anas.ms.repositories.ChannelRepository;
import com.anas.ms.repositories.ChatUserRepository;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.val;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.security.Principal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/channel")
public class ChannelManagementController {
    private static final Logger log = LoggerFactory.getLogger(ChannelManagementController.class);


    @Autowired
    ChannelRepository channelRepository;

    @Autowired
    ChatUserRepository chatUserRepository;

    @PostMapping
    public String addChannel(@RequestBody Channel channel, Principal principal) {
        ChatUser chatUser = chatUserRepository.findByUsernameIgnoreCase(principal.getName());
        if (chatUser.getChannels() == null)
            chatUser.setChannels(new ArrayList<>());
        chatUser.getChannels().add(channel);
        channelRepository.save(channel);

        log.info("Creating new channel {}", channel);
        return "{\"message\":\"added!\"}";
    }

    @PostMapping("/subscribe")
    public String subscribe(@RequestParam(value = "channel") String channelName, Principal principal) {
        Channel channel = channelRepository.findByNameIgnoreCase(channelName);
        if (channel == null)
            throw new ChannelNotFoundException("Channel not found or you're not subscribed");
        ChatUser user = chatUserRepository.findByUsernameIgnoreCase(principal.getName());
        user.getChannels().add(channel);
        channelRepository.save(channel);
        chatUserRepository.save(user);

        log.info("Add user {} to channel {}", principal.getName(), channel);

        return "{\"message\":\"subscribed\"}";
    }

    @GetMapping("/fetch")
    public List<MessageDto> fetch(@RequestParam(value = "channel") String channelName, Principal principal) {
        val channel = getChannel(channelName,
                chatUserRepository.findByUsernameIgnoreCase(principal.getName()));

        log.info("Fetch messages of user {} channel {}", principal.getName(), channelName);

        return channel.getMessages().stream()
                .map(m -> new MessageDto(m.getMessageContent(), m.getSender().getUsername(), m.getTimestamp()))
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.toList());
    }

    @GetMapping("list")
    public List<String> listChannels(Principal principal) {
        val user = chatUserRepository.findByUsernameIgnoreCase(principal.getName());

        log.info("Fetch list user {} channels", principal.getName());

        return user.getChannels().stream().map(Channel::getName).collect(Collectors.toList());
    }

    @GetMapping("list-users")
    public List<String> listUsers(@RequestParam(value = "channel") String channelName, Principal principal) {
        val user = chatUserRepository.findByUsernameIgnoreCase(principal.getName());
        val channel = getChannel(channelName,
                user);

        log.info("Fetch members list for channel {}", channelName);
        return channel.getUsers().stream().map(ChatUser::getUsername).sorted().collect(Collectors.toList());
    }

    private Channel getChannel(String channelName, ChatUser user) {
        val channel = channelRepository.findByNameIgnoreCase(channelName);
        if (channel == null || !user.getChannels().contains(channel))
            throw new ChannelNotFoundException("Channel not found or you're not subscribed");
        return channel;
    }

    @JsonSerialize
    private static class MessageDto implements Comparable<MessageDto>, Serializable {

        @Getter
        private final String content, sender;

        @Getter
        private final Timestamp timestamp;

        public MessageDto(String content, String sender, Timestamp timestamp) {
            this.content = content;
            this.timestamp = timestamp;
            this.sender = sender;
        }


        @Override
        public int compareTo(MessageDto other) {
            return timestamp.compareTo(other.timestamp);
        }
    }
}
