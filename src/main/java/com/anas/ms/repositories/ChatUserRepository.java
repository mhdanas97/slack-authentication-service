package com.anas.ms.repositories;

import com.anas.ms.models.ChatUser;
import org.springframework.data.repository.CrudRepository;

public interface ChatUserRepository extends CrudRepository<ChatUser, Long> {
    ChatUser findByUsernameIgnoreCase(String username);
}
