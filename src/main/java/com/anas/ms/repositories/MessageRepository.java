package com.anas.ms.repositories;

import com.anas.ms.models.Channel;
import com.anas.ms.models.ChatUser;
import com.anas.ms.models.Message;
import org.springframework.data.repository.CrudRepository;

public interface MessageRepository extends CrudRepository<Message, Long> {
    Message findFirstByChannelAndSenderOrderByTimestampDesc(Channel channel, ChatUser user);
}
