package com.anas.ms.repositories;

import com.anas.ms.models.Channel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(exported = false)
public interface ChannelRepository extends CrudRepository<Channel, Long> {
    Channel findByNameIgnoreCase(String name);
}
